# to build on gitlab-ci see https://stackoverflow.com/questions/71632474
[app]
title = MuMi
package.name = mumi
package.domain = org.test
source.dir = .

# ae: removed spec to include buildozer.spec for ae.droid and ae.base
source.exclude_exts = md
source.exclude_dirs = bin, docs, htmlcov, mypy_report, tests
source.exclude_patterns = __pycache__/**, **/__pycache__/**

version.regex = __version__ = ['"](.*)['"]
version.filename = %(source.dir)s/main.py

requirements = android, hostpython3==3.9.6, python3==3.9.6, kivy==2.1.0, plyer, qrcode, kivy_garden.qrcode,
    ae.base, ae.files, ae.paths, ae.deep, ae.droid, ae.inspector, ae.i18n, ae.updater, ae.core, ae.literal, ae.console,
    ae.parse_date, ae.gui_app, ae.gui_help, ae.kivy_auto_width, ae.kivy_dyn_chi, ae.kivy_help, ae.kivy_relief_canvas,
    ae.kivy_app, ae.kivy_glsl, ae.kivy_user_prefs,
    ae.sideloading_server, ae.kivy_sideloading, ae.kivy_file_chooser, ae.kivy_iterable_displayer, ae.kivy_qr_displayer

presplash.filename = %(source.dir)s/img/app_icon.jpg
icon.filename = %(source.dir)s/img/app_icon.jpg
orientation = all

services = Lisz:service.py
fullscreen = 0

android.permissions = INTERNET, READ_EXTERNAL_STORAGE, VIBRATE, WAKE_LOCK, WRITE_EXTERNAL_STORAGE
android.accept_sdk_license = True

# (str) Android entry point, default is ok for Kivy-based app
#android.entrypoint = org.kivy.android.PythonActivity

android.manifest.intent_filters = share_intent_filter.xml

#android.archs = armeabi-v7a, arm64-v8a
android.archs = armeabi-v7a

# ae:08-08-2022: commented out next line to prevent TypeError: cannot pickle '_thread.lock' object
#p4a.branch = develop

# ae:23-03-2022 added next line to ignore setup.py in app projects working tree root; if not only main.py will be copied
# .. but even with this buildozer is still passing --use-setup-py (instead of --ignore-setup-py) to p4a
# so finally removed setup.py from app prj working tree root
#p4a.setup_py = false


[buildozer]
log_level = 2
warn_on_root = 1

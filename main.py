""" mumi music mixer/player.

play music in background (copied from https://groups.google.com/g/kivy-users/c/QUiT2S20QRg/m/BuIUdKK4AQAJ):

* https://stackoverflow.com/questions/45061116/playing-mp3-on-android




"""
from typing import List

from ae.kivy_app import KivyMainApp                                                             # type: ignore
from ae.kivy_sideloading import SideloadingMainAppMixin                                         # type: ignore


__version__ = '0.3.5'


class MumiApp(SideloadingMainAppMixin, KivyMainApp):
    """ app class """
    file_chooser_paths: List[str] = []                      #: store recently used paths as app state for file chooser


# app start
if __name__ in ('__android__', '__main__'):
    MumiApp(app_name='mumi').run_app()
